##
## EPITECH PROJECT, 2017
## Makefile
## File description:
## Compiles program
##

SRC	=	$(SRC_DIR)/main.c	\
		$(SRC_DIR)/create_map.c	\
		$(SRC_DIR)/player_global_function.c	\
		$(SRC_DIR)/player_one_function.c	\
		$(SRC_DIR)/player_two_function.c	\
		$(SRC_DIR)/check_pos_input.c	\
		$(SRC_DIR)/send_signal.c	\
		$(SRC_DIR)/save_signal.c	\
		$(SRC_DIR)/check_victory.c	\
		$(SRC_DIR)/write_ship.c	\
		$(SRC_DIR)/get_next_line.c

SRC_DIR	=	$(realpath ./src)

DEST_A	=	$(realpath ./lib/my)

NAME	=	navy

DEBUG	=	debug

OBJ	=	$(SRC:.c=.o)

CFLAGS	=	-Wall -Wextra	\
		-Iinclude

LFLAGS	=	-L$(DEST_A) -lmy

all:	$(NAME)

$(NAME):	$(OBJ)
	make -C $(DEST_A)
	gcc -o $(NAME) $(OBJ) $(LFLAGS)

debug:		$(OBJ)
	make -C $(DEST_A)
	gcc -o $(DEBUG) -g $(OBJ) $(LFLAGS)

clean:
	rm -f $(OBJ)
	make -C $(DEST_A) clean

fclean:	clean
	rm -f $(NAME)
	rm -f $(DEBUG)
	make -C $(DEST_A) fclean

re:	fclean all
