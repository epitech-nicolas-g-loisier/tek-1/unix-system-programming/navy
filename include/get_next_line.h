/*
** EPITECH PROJECT, 2018
** get_next_line.h
** File description:
** Defines the READ_SIZE macro
*/

#ifndef READ_SIZE
#	define READ_SIZE 16
#endif
