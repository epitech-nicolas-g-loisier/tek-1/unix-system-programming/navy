/*
** EPITECH PROJECT, 2018
** navy
** File description:
** Prototypes the functions used in navy
*/

#include <signal.h>

#ifndef NAVY_
#define NAVY_

char	*get_next_line(int);

int	save_signal(int, pid_t);

int	conv_pos(char *, char *, pid_t);

void	get_signal(int);

void	get_status(int);

int	get_pid(void);

char    *create_empty_map(void);

char    *create_player_map(char*);

char	*check_pos_input(void);

void    print_map(char**);

int	game_loop(int, char**, int);

char	*write_ship(char*, char*, char*, int);

void	get_ennemy_pid(int, siginfo_t *, void *);

char	**attack_turn(int, char**);

char	**defense_turn(int, char **);

int	check_victory(char **);

int	send_pid(int, int);

int	player1_game(char*);

int	player2_game(char*, char*);

void	free_map(char **);
#endif
