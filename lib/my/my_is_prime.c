/*
** EPITECH PROJECT, 2017
** my_is_prime
** File description:
** Returns true if prime
*/

int	my_is_prime(int nb)
{
	int div = 2;

	if (nb < 2)
		return (0);
	while (div < nb) {
		if (nb / div * div == nb)
			return (0);
		div++;
	}
	return (1);
}
