/*
** EPITECH PROJECT, 2017
** my_put_nbr
** File description:
** Prints an int
*/

#include <unistd.h>

void	my_putchar(char c);

int	my_put_nbr(int nb)
{
	int rank = 1;
	int rem;

	if (nb == -2147483648) {
		write(1, "-2147483648", 11);
		return (0);
	}
	if (nb < 0) {
		my_putchar('-');
		nb = nb * -1;
	}
	while (nb / rank >= 10)
		rank = rank * 10;
	while (nb > 9 && rank > 1) {
		rem = nb / rank % rank;
		my_putchar(rem % 10 + 48);
		rank = rank / 10;
	}
	my_putchar(nb % 10 + 48);
	return (0);
}
