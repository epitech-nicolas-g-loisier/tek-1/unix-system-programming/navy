/*
** EPITECH PROJECT, 2017
** my_putstr
** File description:
** Print a string
*/

void	my_putchar(char c);

void	my_putstr(char const *str)
{
	int size = 0;

	while (str[size] != '\0') {
		my_putchar(str[size]);
		size++;
	}
}
