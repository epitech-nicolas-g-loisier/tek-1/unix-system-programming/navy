/*
** EPITECH PROJECT, 2017
** my_sort_int_array
** File description:
** Sorts in increasing order an array of int
*/

void	my_sort_int_array(int *array, int size)
{
	int loop = 0;
	int comp = 0;
	int nbhold = 0;

	while (loop < size + 1) {
		if (array[comp] > array[comp + 1]) {
			nbhold = array[comp];
			array[comp] = array[comp + 1];
			array[comp + 1] = nbhold;
		}
		comp = comp + 1;
		if (comp == size - 1) {
			comp = 0;
			loop = loop + 1;
		}
	}
}
