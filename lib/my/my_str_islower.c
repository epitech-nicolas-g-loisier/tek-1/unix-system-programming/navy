/*
** EPITECH PROJECT, 2017
** my_str_islower
** File description:
** Searches if string is lowercase only
*/

int	my_str_islower(char const *str)
{
	int argc = 0;

	while (str[argc] != '\0') {
		if (str[argc] < 123 && str[argc] > 96)
			argc = argc + 1;
		else
			return (0);
	}
	return (1);
}
