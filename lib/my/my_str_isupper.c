/*
** EPITECH PROJECT, 2017
** my_str_isupper
** File description:
** Searches if string is uppercase only
*/

int	my_str_isupper(char const *str)
{
	int argc = 0;

	while (str[argc] != '\0') {
		if (str[argc] < 91 && str[argc] > 64)
			argc = argc + 1;
		else
			return (0);
	}
	return (1);
}
