/*
** EPITECH PROJECT, 2017
** my_strcat
** File description:
** Concatenates 2 strings
*/

char	*my_strcat(char *dest, char const *src)
{
	int argc = 0;
	int length = 0;

	while (dest[length] != '\0')
		length = length + 1;
	while (src[argc] != '\0') {
		dest[length + argc] = src[argc];
		argc = argc + 1;
	}
	dest[length + argc] = '\0';
	return (dest);
}
