/*
** EPITECH PROJECT, 2017
** my_strcpy
** File description:
** Copies a string into another
*/

char	*my_strcpy(char *dest, char const *src)
{
	int argc = 0;

	while (src[argc] != '\0') {
		dest[argc] = src[argc];
		argc = argc + 1;
	}
	dest[argc] = src[argc];
	return (dest);
}
