/*
** EPITECH PROJECT, 2017
** my_strncmp
** File description:
** Compares the first n bytes of 2 strings
*/

int	my_strncmp(char const *s1, char const *s2, int n)
{
	char s1_v = 0;
	char s2_v = 0;
	int argc = -1;

	while (s1_v - s2_v == 0 && argc < n - 1) {
		argc = argc + 1;
		s1_v = s1[argc];
		s2_v = s2[argc];
		if (s1_v == '\0' && s2_v == '\0')
			return (0);
	}
	return (s1_v - s2_v);
}
