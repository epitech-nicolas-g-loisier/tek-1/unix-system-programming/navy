/*
** EPITECH PROJECT, 2017
** my_strncpy
** File description:
** Copies n char in another string
*/

char	*my_strncpy(char *dest, char const *src, int n)
{
	int argc;
	int length;

	while (src[length] != '\0')
		length = length + 1;
	if (n > length) {
		while (argc <= length) {
			dest[argc] = src[argc];
			argc = argc + 1;
		}
	} else {
		while (argc < n) {
			dest[argc] = src[argc];
			argc = argc + 1;
		}
	}
	return (dest);
}
