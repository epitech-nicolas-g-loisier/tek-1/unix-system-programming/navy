/*
** EPITECH PROJECT, 2017
** my_strstr
** File description:
** Finds first occurence of a pattern in a string
*/

#include <stdio.h>

char	*my_strstr(char const *str, char const *to_find)
{
	int hay_c = 0;
	int need_c = 0;

	if (to_find[need_c] == '\0')
		return ((char *)&str[0]);
	while (str[hay_c] != '\0') {
		if (str[hay_c] == to_find[need_c]) {
			need_c = need_c + 1;
		} else {
			hay_c = hay_c - need_c;
			need_c = 0;
		}
		hay_c = hay_c + 1;
		if (to_find[need_c] == '\0') {
			if (str[hay_c - 1] == to_find[need_c - 1])
				return ((char *)&str[hay_c - need_c]);
		}
	}
	return (NULL);
}
