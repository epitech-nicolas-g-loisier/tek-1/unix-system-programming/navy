/*
** EPITECH PROJECT, 2018
** navy
** File description:
** Checks the input for the shot
*/

#include <stdlib.h>
#include <unistd.h>
#include "navy.h"

int	check_pos(char *buffer)
{
	if (buffer[0] < 'A' || buffer[0] > 'H')
		return (0);
	if (buffer[1] < '1' || buffer[1] > '8' || buffer[2] != '\0')
		return (0);
	return (1);
}

char	*check_pos_input(void)
{
	char *buffer = NULL;
	int valid_input = 0;

	while (!valid_input) {
		write(1, "attack: ", 8);
		buffer = get_next_line(0);
		if (buffer == NULL)
			return (NULL);
		valid_input = check_pos(buffer);
		if (!valid_input) {
			write(1, "wrong position\n", 15);
			free(buffer);
		}
	}
	return (buffer);
}
