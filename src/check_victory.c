/*
** EPITECH PROJECT, 2018
** navy
** File description:
** Checks if any of the player won
*/

int	count_hit(char *map)
{
	int count = 0;
	int hit_count = 0;

	while (map[count] != '\0') {
		if (map[count] == 'x')
			hit_count++;
		count++;
	}
	if (hit_count != 14)
		return (1);
	return (0);
}

int	check_victory(char **map)
{
	int count = 0;

	while (count < 2 && count_hit(map[count]))
		count++;
	return ((count + 1) % 3);
}
