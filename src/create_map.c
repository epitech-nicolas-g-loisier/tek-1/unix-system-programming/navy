/*
** EPITECH PROJECT, 2018
** Navy
** File description:
** create_map
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include "my.h"
#include "navy.h"

char	*create_empty_map(void)
{
	char str[181] = " |A B C D E F G H\n-+---------------\n" \
			"1|. . . . . . . .\n2|. . . . . . . .\n" \
			"3|. . . . . . . .\n4|. . . . . . . .\n" \
			"5|. . . . . . . .\n6|. . . . . . . .\n" \
			"7|. . . . . . . .\n8|. . . . . . . .\n\0";
	char *map = my_strdup(str);

	return (map);
}

int	find_way(char *pos1, char *pos2)
{
	int pos_1 = 0;
	int pos_2 = 0;
	int way = 0;

	if (pos1[4] != ':' || pos2[2] != '\n') {
		return (0);
	} else if (pos1[2] < 'A' || pos1[2] > 'H' ||
			pos2[0] < 'A' || pos2[1] > 'H') {
		return (0);
	} else
		if (pos1[3] < '1' || pos1[3] > '9' ||
			pos2[0] < '1' || pos2[1] > '9') {
			return (0);
	}
	pos_1 = 36 + (pos1[2] - '@') * 2 + (pos1[3] - '1') * 18;
	pos_2 = 36 + (pos2[0] - '@') * 2 + (pos2[1] - '1') * 18;
	way = (pos_2 - pos_1) / (pos1[0] - '1');
	if (((pos1[0] - '1') * way + pos_1) != pos_2)
		way = 0;
	return (way);
}

char	*add_ship(int fd, char *map)
{
	int way = 0;
	int count_arg = 0;
	char pos1[6];
	char pos2[4];

	while (count_arg < 4 && read(fd, pos1, 5) != -1) {
		pos1[5] = 0;
		read(fd, pos2, 3);
		pos2[3] = 0;
		way = find_way(pos1, pos2);
		if (pos1[1] != ':' || way == 0 || map == NULL){
			write(2, "Error: invalid file\n", 20);
			free(map);
			return (NULL);
		}
		map = write_ship(pos1, pos2, map, way);
		count_arg++;
	}
	return (map);
}

char	*create_player_map(char *file)
{
	int fd = open(file, O_RDONLY);
	char *map = create_empty_map();

	if (map == NULL) {
		return (NULL);
	} else if (fd == -1) {
		write(2, "Error: can't open file\n", 23);
		free(map);
		return NULL;
	}
	map = add_ship(fd, map);
	close(fd);
	return (map);
}

void	free_map(char **map)
{
	if (map != NULL){
		if (map[0] != NULL)
			free(map[0]);
		if (map[1] != NULL)
			free(map[1]);
	}
}
