/*
** EPITECH PROJECT, 2018
** Navy
** File description:
** Battleship game
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include "my.h"
#include "navy.h"

int	send_pid(int sig, int pid)
{
	static int save_pid = 0;

	if (sig == SIGUSR1) {
		save_pid = pid;
	}
	if (sig == 0){
		pause();
	}
	return (save_pid);
}

void	get_ennemy_pid(int sig, siginfo_t *info, void *old)
{
	old++;
	send_pid(sig, info->si_pid);
}

void	print_usage(void)
{
	write(1, "USAGE\n", 6);
	write(1, "\t./navy [first_player_pid] navy_positions\n", 42);
	write(1, "DESCRIPTION\n", 12);
	write(1, "\tfirst_player_pid: only for the 2nd player. ", 44);
	write(1, "pid of the first player.\n", 25);
	write(1, "\tnavy_positions: file representing ", 35);
	write(1, "the positions of the ships.\n", 28);
}

int	display_victory(int ret)
{
	if (ret == 1) {
		write(1, "I won\n", 6);
		return (0);
	}
	else if (ret == 0) {
		write(1, "Enemy won\n", 10);
		return (1);
	}
	else {
		return (ret);
	}
}

int	main(int argc, char **argv)
{
	int ret = 0;

	if (argc == 2) {
		if (my_strcmp(argv[1], "-h") == 0)
			print_usage();
		else
			ret = player1_game(argv[1]);
	} else if (argc == 3) {
		ret = player2_game(argv[1], argv[2]);
	} else {
		write(2, "Error: Invalid numder of argument\n", 34);
		write(2, "Retry with -h\n", 14);
		return (84);
	}
	ret = display_victory(ret);
	return (ret);
}
