/*
** EPITECH PROJECT, 2018
** Navy
** File description:
** Player_function
*/

#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include "my.h"
#include "navy.h"

char	*check_pos_hit(char *map, int pos, int pid)
{
	char pos_char = 'A' + ((pos - 38) - (pos - 38) / 18 * 2) % 16 / 2;

	write(1, &pos_char, 1);
	pos_char = '1' + (pos - 38) / 18;
	write(1, &pos_char, 1);
	if (map[pos] >= '2' && map[pos] <= '5') {
		map[pos] = 'x';
		write(1, " : hit\n", 7);
		kill(pid, SIGUSR2);
	} else {
		if (map[pos] == '.')
			map[pos] = 'o';
		write(1, " : missed\n", 10);
		kill(pid, SIGUSR1);
	}
	return (map);
}

int	get_pid(void)
{
	int pid = getpid();

	write(1, "my_pid: ", 8);
	my_put_nbr(pid);
	write(1, "\n", 1);
	return (pid);
}

void	print_map(char **map)
{
	write(1, "my positions:\n", 14);
	write(1, map[0], 180);
	write(1, "\n", 1);
	write(1, "enemy's positions:\n", 19);
	write(1, map[1], 180);
	write(1, "\n", 1);
}

char	**attack_turn(int pid, char **map)
{
	char *attack;
	struct sigaction send;

	sigaction(SIGUSR1, NULL, &send);
	send.sa_handler = &get_status;
	attack = check_pos_input();
	if (attack == NULL) {
		free_map(map);
		return (NULL);
	}
	sigaction(SIGUSR1, &send, NULL);
	sigaction(SIGUSR2, &send, NULL);
	conv_pos(attack, &map[1][0], pid);
	free(attack);
	return (map);
}

char	**defense_turn(int pid, char **map)
{
	int pos = 0;
	struct sigaction receive;

	sigaction(SIGUSR1, NULL, &receive);
	receive.sa_handler = &get_signal;
	write(1, "waiting for enemy's attack...\n", 30);
	sigaction(SIGUSR1, &receive, NULL);
	sigaction(SIGUSR2, &receive, NULL);
	pos = save_signal(0, pid);
	if (pos == -1){
		free_map(map);
		return (NULL);
	}
	map[0] = check_pos_hit(map[0], pos, pid);
	write(1, "\n", 1);
	return (map);
}
