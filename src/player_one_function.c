/*
** EPITECH PROJECT, 2018
** Navy
** File description:
** Function for palyer1 game
*/

#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include "navy.h"

int	player1_loop(int pid, char **map)
{
	int win = 0;
	int turn = 1;

	write(1, "enemy connected\n\n", 17);
	while (win == 0) {
		if (turn == 1) {
			print_map(map);
			map = attack_turn(pid, map);
			turn = 0;
		} else {
			map = defense_turn(pid, map);
			turn = 1;
		}
		if (map == NULL)
			return (84);
		win = check_victory(map);
	}
	print_map(map);
	free_map(map);
	return ((win - 1));
}

int	player1_game(char *file)
{
	char *map[2];
	int win = 0;
	int ennemy_pid = 0;
	struct sigaction info;

	sigaction(SIGUSR1, NULL, &info);
	info.sa_flags = SA_SIGINFO;
	info.sa_sigaction = &get_ennemy_pid;
	map[0] = create_player_map(file);
	map[1] = create_empty_map();
	if (map[0] == NULL || map[1] == NULL) {
		free_map(map);
		return (84);
	}
	get_pid();
	write(1, "waiting for enemy connection...\n\n", 33);
	sigaction(SIGUSR1, &info, NULL);
	ennemy_pid = send_pid(0, 0);
	win = player1_loop(ennemy_pid, map);
	return (win);
}
