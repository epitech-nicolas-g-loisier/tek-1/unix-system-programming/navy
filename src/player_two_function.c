/*
** EPITECH PROJECT, 2018
** Navy
** File description:
** function for player2 game
*/

#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include "navy.h"
#include "my.h"

int	player2_loop(int pid, char **map)
{
	int win = 0;
	int turn = 0;

	while (win == 0) {
		if (turn == 1) {
			map = attack_turn(pid, map);
			turn = 0;
		}
		else {
			print_map(map);
			map = defense_turn(pid, map);
			turn = 1;
		}
		if (map == NULL)
			return (84);
		win = check_victory(map);
	}
	print_map(map);
	free_map(map);
	return ((win - 1));
}

int	player2_game(char *pid, char *file)
{
	char *map[2];
	int win = 0;
	int ennemy_pid = my_getnbr(pid);

	map[0] = create_player_map(file);
	map[1] = create_empty_map();
	if (map[0] == NULL || map[1] == NULL) {
		free_map(map);
		return (84);
	}
	get_pid();
	kill(ennemy_pid, SIGUSR1);
	write(1, "successfully connected\n\n", 24);
	win = player2_loop(ennemy_pid, map);
	return (win);
}
