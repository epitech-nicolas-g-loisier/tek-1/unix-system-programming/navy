/*
** EPITECH PROJECT, 2018
** navy
** File description:
** Saves the signal caught
*/

#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "navy.h"

void	conv_binary(int *pos, int signals[6])
{
	char value = 0;

	value += signals[3] * 16 + signals[4] * 32 + signals[5] * 64;
	value = value + value / 16 * 2;;
	*pos = value;
	value = 38;
	value += signals[0] * 2 + signals[1] * 4 + signals[2] * 8;
	*pos += value;
}

int	conv_sig(int sig)
{
	if (sig == SIGUSR2)
		return (1);
	return (0);
}

int	save_signal(int sig, pid_t pid)
{
	static int signals[6];
	static int signal_c = 0;
	int pos;

	if (signal_c < 6 && (sig == SIGUSR1 || sig == SIGUSR2)) {
		signals[signal_c++] = conv_sig(sig);
		return (0);
	}
	while (signal_c < 6 && sig == 0) {
		if (pid != 0 && kill(pid, 0) == -1)
			return (-1);
		usleep(100);
	}
	if (sig == 0) {
		conv_binary(&pos, signals);
		signal_c = 0;
	}
	return (pos);
}

void	get_signal(int sig)
{
	save_signal(sig, 0);
}
