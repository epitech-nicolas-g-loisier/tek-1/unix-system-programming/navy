/*
** EPITECH PROJECT, 2018
** navy
** File description:
** Sends the position using signals to the process
*/

#include <signal.h>
#include <unistd.h>
#include "navy.h"

void	print_shot_status(char *map, char *hit_pos, int status)
{
	int pos = 36 + (hit_pos[0] - '@') * 2 + (hit_pos[1] - '1') * 18;

	write(1, hit_pos, 2);
	if (status == SIGUSR1) {
		write(1, ": missed\n\n", 10);
		if (map[pos] == '.')
			map[pos] = 'o';
	} else {
		write(1, ": hit\n\n", 7);
		map[pos] = 'x';
	}
}

int	get_shot_status(char *map, char *pos, int sig, int pid)
{
	static int status = -1;

	if (sig == SIGUSR1 || sig == SIGUSR2)
		status = sig;
	while (map != NULL && status == -1) {
		if (kill(pid, 0) == -1)
			return (84);
		usleep(100);
	}
	if (map != NULL) {
		print_shot_status(&map[0], pos, status);
		status = -1;
	}
	return (0);
}

int	send_signals(int *signals, pid_t pid, char *map, char *pos)
{
	int sigtype[2] = {SIGUSR1, SIGUSR2};
	int count = 0;

	while (count < 6) {
		if (kill(pid, sigtype[signals[count]]) == -1)
			return (84);
		usleep(1000);
		count++;
	}
	return (get_shot_status(&map[0], pos, 0, pid));
}

int	conv_pos(char *pos, char *map, pid_t pid)
{
	char pos_cpy[2] = {pos[0], pos[1]};
	int signals[6] = {0, 0, 0, 0, 0, 0};
	int coef[3] = {1, 2, 4};
	int count = 5;

	while (count >= 0) {
		if (count > 2) {
			signals[count] = (pos[1] - '1') / coef[count % 3];
			pos[1] = (pos[1] - '1') % coef[count % 3] + '1';
		} else {
			signals[count] = (pos[0] - 'A') / coef[count % 3];
			pos[0] = (pos[0] - 'A') % coef[count % 3] + 'A';
		}
		count--;
	}
	return (send_signals(signals, pid, &map[0], pos_cpy));
}

void	get_status(int sig)
{
	get_shot_status(NULL, NULL, sig, 0);
}
