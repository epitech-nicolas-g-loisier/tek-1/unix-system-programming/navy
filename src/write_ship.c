/*
** EPITECH PROJECT, 2018
** Navy
** File description:
** create_map
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include "my.h"
#include "navy.h"

char	*reverse_put_ship(char *pos1, char *pos2, char *map, int way)
{
	int idx = 36 + (pos1[0] - '@') * 2 + (pos1[1] - '1') * 18;

	while (idx <= 36 + (pos2[2] - '@') * 2 + (pos2[3] - '1') * 18) {
		if (map[idx] != '.' || (way != 2 && way != 18)) {
			free(map);
			return (NULL);
		}
		map[idx] = pos2[0];
		idx += way;
	}
	return map;
}

char	*put_ship(char *pos1, char *pos2, char *map, int way)
{
	int idx = 36 + (pos1[2] - '@') * 2 + (pos1[3] - '1') * 18;

	while (idx <= 36 + (pos2[0] - '@') * 2 + (pos2[1] - '1') * 18) {
		if (map[idx] != '.' || (way != 2 && way != 18)) {
			free(map);
			return (NULL);
		}
		map[idx] = pos1[0];
		idx += way;
	}
	return map;
}

char	*write_ship(char *pos1, char *pos2, char *map, int way)
{
	int check = 0;
	static char ship_size[4] = "2345";

	while (check < 4) {
		if (ship_size[check] == pos1[0]){
			ship_size[check] = 0;
			break;
		}
		check++;
	}
	if (check == 4) {
		free(map);
		return (NULL);
	}
	if (way > 0){
		map = put_ship(pos1, pos2, map, way);
	}
	else {
		map = reverse_put_ship(pos2, pos1, map, -way);
	}
	return map;
}
